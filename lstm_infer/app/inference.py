"""
    Inference service for Long short-term memory(lstm) model
"""

from __future__ import print_function

import json
import os
import pickle

import numpy as np
from app.create_filename import create_filename
from flask import Flask, request, Response
from keras.models import load_model
from keras.preprocessing.sequence import pad_sequences
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Sanyog Vyawahare"

logging = XprLogger("lstm_infer")

cur_work_dir = os.getcwd()
app = Flask(__name__)

config = json.load(open(os.path.abspath(os.path.join(cur_work_dir, 'config/config.json'))))

use_gpu_bool = config['lstm']['use_gpu']
num_of_gpu = len(config['lstm']['cuda_visible_devices'].split(","))

if use_gpu_bool:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = config['lstm']['cuda_visible_devices']
    if num_of_gpu == 1:
        config['lstm']['use_gpu'] = False
else:
    os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"] = " "

filename = config['data_preprocess']['input_file'].split('/')[-1].split('.')[0]
master_path = config['data_preprocess']['master_path']
master_path = master_path.replace("FILE", filename)
master_path = master_path.replace("CUTTING", str(config['data_preprocess']['cutting_window']))
master_path = master_path.replace("SPLIT", str(config['data_preprocess']['train_split_ratio']))
master_path = master_path.replace("RW", str(config['data_preprocess']['use_read_write_operation_feature']))
master_path = str(master_path)


class LstmInfer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        pass

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pass


@app.route("/lstm_inference", methods=["POST"])
def inference():
    request_body = request.get_json()
    response = dict()
    logging.info(f"Request Body : {str(request_body)} \n")
    if 'InputSequence' not in request.get_json() or 'Topk' not in request.get_json():
        return Response(
            status=404,
            response="Required Parameters not found"
        )
    try:
        input_sequence = request_body['InputSequence']
        k = int(request_body['Topk'])
        input_sequence = input_sequence.split()
        input_sequence_tokenized = tokenizer.texts_to_sequences([input_sequence])[0]
        input_sequence_padded = pad_sequences([input_sequence_tokenized], maxlen=max_length - 1, padding='pre')

        predicted_ids = np.ndarray.tolist(np.argsort(model.predict(input_sequence_padded), axis=1))[0]
        predicted_ids.reverse()
        resp_seq = list()

        for index, value in enumerate(predicted_ids):
            if index >= k:
                break
            resp_seq.append(index_to_id[value])
        return Response(
            status=200,
            response=json.dumps({"predicted sequences": resp_seq})
        )

    except Exception as e:
        return Response(status=200, response=str(e))


def main():
    app.run(host='0.0.0.0', port=8000, threaded=False, debug=False, use_reloader=False)


if __name__ == "__main__":

    logging.info("Loading Tokenizer...")
    # Loading tokenizer
    tokenizer_filename = "tokenizer" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], tokenizer_filename)), 'rb') as handle:
        tokenizer = pickle.load(handle)
    index_to_id = {v: k for k, v in tokenizer.word_index.items()}
    logging.info("Tokenizer Loading Completed")

    # Loading max length
    logging.info("Loading Max Sequence Length...")
    max_length_filename = "maxlength" + create_filename(config) + ".pickle"
    with open(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                           config['lstm']['pickle_path'], max_length_filename)), 'rb') as handle:
        max_length = pickle.load(handle)
    handle.close()
    logging.info("Max Sequence Length Loading Completed")

    # Loading model
    logging.info("Loading model ...")
    model_name = "model" + create_filename(config) + ".h5"
    if os.path.exists(os.path.abspath(os.path.join(cur_work_dir, config['data_preprocess']['mounted_path'], master_path,
                                                   config['lstm']['model_save_path'], model_name))):
        model = load_model(os.path.abspath(
            os.path.join(cur_work_dir, str(config['data_preprocess']['mounted_path']), master_path,
                         str(config['lstm']['model_save_path']), str(model_name))))

        use_gpu_bool = config["lstm"]["use_gpu"]
    logging.info("Model loading completed")
    main()
