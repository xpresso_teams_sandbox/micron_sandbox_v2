""" 
    Probability graph API module
"""

import json
import pickle

from flask import Flask
from flask_restful import Resource, Api, reqparse
from xpresso.ai.core.data.inference import AbstractInferenceService
from xpresso.ai.core.logging.xpr_log import XprLogger

__author__ = "Sanyog Vyawahare"

logging = XprLogger("bn_infer")

APP = Flask(__name__)
API = Api(APP)


# Model loading function
def load_model(filename):
    """ Loading graph """
    input_dict = open(filename, 'rb')
    data = pickle.load(input_dict)
    input_dict.close()
    return data


# Getting model data
def load_test_file_helper():
    """ helper function for test sequence generation """
    master_dict = {}
    for item in RANGE_LIST:
        logging.info(f"Loading file {str(item)}")
        print("Loading file", str(item))
        filename = BN_SPLIT.replace('VAL', str(item))
        master_dict[str(item)] = load_model(filename)
    return master_dict


# Getting prediction
def get_pred(key_val, item3):
    """ Getting prediction of sequence """
    pred = []
    key = '_'.join([str(item2) for item2 in key_val])
    if key in MODELDATA[item3]:
        pred = MODELDATA[item3][key]
    return pred


# Getting final prediction
def get_model_output(block, neighbor):
    """ Get sequence and return the predicted sequence """
    if len(block) < LOOKAHEAD:
        block = [-1] * (LOOKAHEAD - len(block)) + block
    else:
        block = block[-LOOKAHEAD:]

    predict = []
    for item in RANGE_LIST:
        key = list(block)[-item:]
        predict += get_pred(key, str(item))
        if len(predict) >= neighbor:
            break
    return predict[:neighbor]


# Probability graph class for GUI
class BNetworkPredict(Resource):
    """ Declaring and defining class objects """

    def post(self):
        """ Method for POST response """
        try:
            PARSER.add_argument('Topk', type=str)
            PARSER.add_argument('InputSequence', type=str)
            args = PARSER.parse_args()
            block = args['InputSequence'].split(' ')
            neighbor = int(args['Topk'])
            logging.info(f'Input {block}')
            logging.info(f'TopK {neighbor}')
            print(args)
            final_output = get_model_output(block, neighbor)
            logging.info(f'Output {final_output}')
            print(f'Block_Sequence: {final_output}')
            return {'Block_Sequence': final_output}, 200
        except Exception:
            final_output = "Internal Server Error"
            logging.info(f'Output {final_output}')
            return final_output, 500


class BnInfer(AbstractInferenceService):
    """ Main class for the inference service. User will need to implement
    following functions:
       - load_model: It gets the directory of the model stored as parameters,
           user will need to implement the method for loading the model and
           updating self.model variable
       - transform_input: It gets the JSON object from the rest API as input.
           User will need to implement the method to convert the json data to
           relevant feature vector to be used for prediction
       - predict: It gets the feature vector or any other object from the
           transform_input method. User will need to implement the model
           prediction codebase here. It returns the predicted value
       - transform_output: It gets the predicted value from the predict method.
           User need to implement this method to converted predicted method
           to JSON serializable object. Response from this method is send back
           to the client as JSON Object.

    AbstractInferenceService automatically creates the flask reset api
    with resource /predict for this class.
    Request JSON format:
       {
         "input" : <input_goes_here> // It could be any JSON object
       }
       This value of "input" key is sent to transform_input

    Response JSON format:
       {
         "message": "success/failure",
         "results": <response goes here> // It could be any JSOn object
       }
       Output of transform_output goes as value of "results"
    """

    def __init__(self):
        super().__init__()
        """ Initialize any static data required during boot up """

    def load_model(self, model_path):
        """ This is used to load the model on the boot time.
        User will need to load the model and save it in the variable
        self.model. It is IMPORTANT to update self.model.
        Args:
            model_path(str): Path of the directory where the model files are
               stored.
        """
        pass

    def transform_input(self, input_request):
        """
        Convert the raw input request to a feature data or any other object
        to be used during prediction

        Args:
            input_request: JSON Object or an array received from the REST API,
               which needs to be converted into relevant feature data.

        Returns:
            obj: Any transformed object
        """
        pass

    def predict(self, input_request):
        """
        This method implements the prediction method of the supported model.
        It gets the output of transform_input as input and returns the predicted
        value

        Args:
            input_request: Transformed object outputted from transform_input
               method

        Returns:
            obj: predicted value from the model

        """
        pass

    def transform_output(self, output_response):
        """
        Convert the predicted value into a relevant JSON serializable object.
        This is sent back to the REST API call
        Args:
            output_response: Predicted output from predict method.

        Returns:
          obj: JSON Serializable object
        """
        pass


PARSER = reqparse.RequestParser()
API.add_resource(BNetworkPredict, '/bn_predict_block')

if __name__ == '__main__':
    # Reading config data from json
    CONFIG_PATH = 'config/config.json'
    CONFIG_FILE = open(CONFIG_PATH, 'r')
    JSON_OBJECT = json.load(CONFIG_FILE)
    CONFIG_FILE.close()

    DATA_MODEL = JSON_OBJECT['model']['pg_bn']
    DATA_PREP_PARAM = JSON_OBJECT['data_preprocess']

    INPUTFILE = DATA_PREP_PARAM['input_file']

    READ_WRITE_FLAG = DATA_PREP_PARAM['use_read_write_operation_feature']
    SPLITPERC = DATA_PREP_PARAM['train_split_ratio']
    CUTTING_WINDOW = DATA_PREP_PARAM['cutting_window']

    FILENAME = str(INPUTFILE.split('/')[-1].split('.')[0])
    MASTERPATH = DATA_PREP_PARAM['master_path']
    MASTERPATH = MASTERPATH.replace("FILE", FILENAME)
    MASTERPATH = MASTERPATH.replace("CUTTING", str(CUTTING_WINDOW))
    MASTERPATH = MASTERPATH.replace("SPLIT", str(SPLITPERC))
    MASTERPATH = MASTERPATH.replace("RW", str(READ_WRITE_FLAG))
    MOUNTEDPATH = DATA_PREP_PARAM['mounted_path'] + MASTERPATH
    MODELPATH = MOUNTEDPATH + DATA_MODEL['model_path_bn']

    LOGPATH = MODELPATH + DATA_PREP_PARAM['logfile_inference']
    LOOKAHEAD = DATA_MODEL['lookahead_window']
    RANGE_LIST = range(LOOKAHEAD, 0, -1)
    BN_SPLIT = MODELPATH + DATA_MODEL['output_files']['bn_split']

    logging.info("Process started")
    print("Process started")

    logging.info("Model loading... ")
    print("Model loading... ")
    MODELDATA = load_test_file_helper()
    logging.info("Model loading Completed")
    print("Model loading Completed")

    HOSTNAME = DATA_MODEL['hostname']
    PORT = DATA_MODEL['bn_api_port']
    APP.run(host=HOSTNAME, port=PORT)
